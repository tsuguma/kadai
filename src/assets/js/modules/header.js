import $ from 'jquery';
export class Header {
  init() {
    this.headerMenu();
  }

  headerMenu() {
    $('.js-header-trigger').on('click', function () {
      $('.js-header-nav').slideToggle();
      $(this).toggleClass('is-active');
    });
  }
}