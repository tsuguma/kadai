import $ from 'jquery';
import SlickCarousel from '../../../../node_modules/slick-carousel/slick/slick.js';

export class InitSlick {
  init() {
    this.slickSlider();
  }

  slickSlider() {
    $('.js-slider').slick({
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      fade: true,
    });

  }
}