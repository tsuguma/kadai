import { Header } from "./modules/header";
import { InitSlick } from "./modules/init-slick";

const header = new Header();
header.init();

const initSlick = new InitSlick();
initSlick.init();