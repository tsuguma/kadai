# Project Name

- Node.js v.14.15.4
- yarn

- HTML5
- SCSS
- jQuery


## Setup

```
yarn
```

```
yarn start
```

## Directory
- src(開発ディレクトリ)
- public_html(公開ディレクトリ)

## Lint
stylelint, eslint使用
ルールはプロジェクトごとに適宜変更して使用する。

### EditorConfig
各エディタでプラグインをインストールしてください。
参考: https://qiita.com/naru0504/items/82f09881abaf3f4dc171
